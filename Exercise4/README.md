# Exercise 4

Task: Create two  vagrant systems: System A and System B.

>**Instruction**

-Create an ssh key,copy the public key from system A to system B
-Have access to system B by usung ssh command.

## Solution

>I named system A **Master** and system B **Slave**

>A screen shot of logging into slave and ipaddress is shown in the screenshot below:  

![slaveipaddress](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise4/images/slaveipaddress.png)  

>A screen shot of generating sshkey for the master syster is hereby attached in the screenshot below :  

![sshkeymaster](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise4/images/sshkeymaster.png)

>A screenshot of the changes made in vagrant file on public key and password authentication is hereby attached below :  

![changesinvagrantfilesettins](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise4/images/changesettingsinvagrant.png)

>A screenshot of public key copied into _authorized keys_ file of slave machine is attached below :  

![publickeyinslavemachine](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise4/images/publickeycopiedintoslave.png)

>A screenshot of vagrant machine signed in ip address and the two machines running is attached below:  

![loginwithipaddress](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise4/images/loginintoslave.png)
