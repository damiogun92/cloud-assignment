#EXRERCISE 2

>TASKS


>Research online for 10 more linux commands asides the ones already mentioned in this module.
>submit using your alt-school-cloud-exercises project
>Explain what each command is used for with examples on how how to each and screenshots >using each of them.


#SOLUTION

>10 OTHER LINUX COMMANDS

`sort` : 

>The sort command helps to arrange lists of items alpbabetically


![sort commandoutput](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/sortcommand.png?raw=true)

`diff` : 

>The diff command is used to process similar files and tell the differeance between the two fileds.Example of a diff out is shown in the screenshot below:

![diff commandoutput](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/diffcommand.png?raw=true)

`which` : 

>This is used when you have a command in a shell path and want to know where it is located.The which command will return the path to the command specified.Example of the which command is shown in the screenshot below:

 ![which command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/whichcommand.png?raw=true)

`whatis` : 

>The whatis command is used to print a single line of description of another command,making it a helpful reference.Example of how whatis command is used in the screenshot below:

 ![what is command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/whatiscommand.png?raw=true)

`wc` : 

>This command stands for word count and as the name suggests it retuens the number of words in a text file.Example of how it is used is seen in the screenshot below:

![wc command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/wc%20command.png?raw=true)

`wget` : 

>This command is a utility to retrieve content from the internet.It has one of the largest collection of the files out there.Example of how wget command is seen in the screen shot below

![wget output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/wgetcommand.png?raw=true)

`find` : 

>The find command searches for files in a directory hierarchy based on a regex expression.Example of the find command is shown in the  screenshot below:

![find command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/findcommand.png?raw=true)

`cal` : 

>The command shows the calender of the month.Example of how cal command is used is seen in the screenshot below:

![cal command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/calcommand.png?raw=true)

`w` : 

>This command shows the user that is online. Example of how w command is used is seen in the screenahot below:

![w command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/wcommand.png?raw=true)

`free` : 

>This command shows the free memory availaible.Example of how free command is used is seen in the screenshot below:

![free command output](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise2/images/findcommand.png?raw=true)
