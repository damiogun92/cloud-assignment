## TASKS


>Install php7.4 on your local linux machine using the ppa:ondrej/php package repo


### INSTRUCTION


* Learn how to use add-apt-repository command  

* Submit the content of your /etc/apt/sources.list and output of php -v .


## SOLUTION

`ADDING A DEPENDENCY AND REPOSITORY`:

I added a dependency using **sudo app-get install software-properties -common apt-transport https -y** then, I went further to add a repository using **sudo add-apt-repository ppa:ondre/php -y** command.See screenshot below:

![add repository](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise5/Images/addaptrepository%20png.png)

`RUNNING AN UPDATE AND UPGRADE` : 

>Because i added a new repository,for it to work properly i needed to run an update and upgrade.The screenshot of the shown below:

![Runupdateandupgrade](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise5/Images/displayaptupdateandupgrade%20png.png)

`TO INSTALL PHP ` : 

>I installed php  with basic add-ons using the command **Sudo apt-get install -y php7.4 php7.4 -common** screenshot below:

 ![Displayphp-v](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise5/Images/display%20php-v%20png.png)

`TO DISPLAY \ETC\APT\SOURCES.LIST` :   

>To display the php package installed, i used cat /etc/apt/sources.list but since i back up the file i displayed it using the command cat /etc/apt/sources.list.backup.

![display\etc\apt\source.list](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise5/Images/display:etc:apt:sources.list%20png.png)