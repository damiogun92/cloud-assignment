# EXERCISE 3

>TASKS


>Create 3 groups - admin,support and engineering and add the admin group to sudoers.
>
>create a user in each of the group.
>
>Generate ssh-keys to the user in the admin group.

## INSTRUCTION

>submit the contents of :
>/etc/passwd
>/etc/group
>/etc/sudoers

# SOLUTION


`ADMIN GROUP`

>To create the admin group i used the useradd (username) -G group command.I named the user cloud1 and named the group admin as instructed.I displayed the output using cat /etc/passwd.Kindly see the screenshot below: 

![groupadminwithcloud1user](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/groupadminusercloud1.png?raw=true)


`SUPPORT GROUP`

>To create the support group,i went differently and i added the user with useradd and the group with groupadd.I named the user cloud2 and group support.I assigned the user to the group using usermod -G (group) (username).I displayed the output using cat /etc/passwd.Kindly see screenshot below:

![groupsupportwithcloud2user](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/groupsupportusercloud2.png?raw=true)


`DISPLAYING ADMIN AND SUPPORT GROUP`

>I then displayed group for both group admin and group support using the cat /etc/group.
Kindly see screenshot below:

![cat /etc/group](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/cat%20:etc:group.png?raw=true)


`ENGINEERING GROUP`
>Lastly i created group engineering using group add command and user cloud3 usig useradd command.I displayed the output using cat /etc/passwd

![groupengineeringwithusercloud3](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/groupengineeringusercloud3.png?raw=true)


`DISPLAYING ENGINEERING GROUP`

>I displayed the engineering group using cat /etc/group.Kindly see screenshot below:

![displayengineeringgroup](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/cat%20:etc:groupofengineering.png?raw=true)

`ADD ADMIN GROUP TO SUDOER`

`LOGIN INTO USER (CLOUD1) FOR ADMIN GROUP,ADD ADMIN GROUP AND DISPLAY SUDOERS`

>i tried to add admin group to sudoers by editing the sudo file using the commmand " visudo /etc/sudoers.The admin group was already in the file so i did not edit the sudo file.I then displayed the sudoeers file using cat /etc/sudoers
> While still being a root user,i did used an su command along with the username but it denied permision so i had to modify user and add it to the sudoer using "sudo usermod -aG admin cloud1" and i was able to log in user as a sudoer.Kindly see screenshot below:

![displayloginanddisplaysudoer](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/loginintousercloud1.png?raw=true)

`SSH-KEYGEN FOR USER (CLOUD1`

>I generated ssh key for user cloud1 for the admin group using sudo ssh-keygen command.
kindly see screenshot below;

![displaysshkeyforuseradmin](https://github.com/damiogun92/cloud-assignment/blob/main/Exercise3/IMAGES/sshkeygenuseradmin.png?raw=true)

